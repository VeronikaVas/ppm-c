## PPM-C
Simple implementation of Prediction with Partial Match (PPM) - C variant with [Huffmann coding with priority queues](https://courses.cs.washington.edu/courses/cse143x/15au/lectures/huffman/huffman.pdf).

### How it works
To Do

### How to run

To encode a file:

```
java -jar ppmc.jar -c <file_in> <file_out>
```

To decode:

```
java -jar ppmc.jar -d <file_in> <file_out>
```
The requirements: JRE 1.8.

### Resources:
* https://phoenix.inf.upol.cz/esf/ucebni/komprese.pdf
* https://people.cs.nctu.edu.tw/~cmliu/Courses/Compression/chap6.pdf
* http://www.stringology.org/DataCompression/ppmc/index_cs.html
* https://www.vutbr.cz/www_base/zav_prace_soubor_verejne.php?file_id=117690
