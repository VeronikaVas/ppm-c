package ppmc;

import java.util.ArrayList;
import java.util.List;

/*Circular buffer for storing actual context*/

public class Context implements Cloneable{

    byte[] buffer;
    int pos;

    public Context(int k) {
        buffer = new byte[k];
        pos = 0;
    }

    public int order() {
        return pos;
    }

    public void reduce() throws IndexOutOfBoundsException {
        if (buffer.length == 0) {
            return;
        }

        for (int i = 0; i < pos - 1; i++) {
            buffer[i] = buffer[i + 1];
        }
        pos--;
    }

    public void clear() {
        pos = 0;
    }

    public void append(byte a) {
        if (buffer.length == 0) {
            return;
        }
        if (pos == buffer.length) {
            int max = pos - 1;
            for (int i = 0; i < max; i++) {
                buffer[i] = buffer[i + 1];
            }
            buffer[max] = a;
        } else {
            buffer[pos++] = a;
        }
    }

    public byte get(int p) {
        return buffer[p];
    }
    
    public Context clone() {

        Context c = new Context(buffer.length);
        c.buffer = buffer.clone();
        c.pos = pos;

        return c;

    }
    
    
    public static class Exclusion {

        List<Byte> excluded = new ArrayList<>();

        public void clear() {
            excluded.clear();
        }

        public void exclude(byte b) {
            if (!excluded.contains(b)) {
                excluded.add(b);
            }
        }

        public boolean isExcluded(byte b) {
            return excluded.contains(b);
        }
    }

}
