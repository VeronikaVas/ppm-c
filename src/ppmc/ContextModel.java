package ppmc;

import java.util.ArrayList;
import java.util.List;
import ppmc.Context.Exclusion;

/**
 *
 * @author Veronika Vasinova
 */
public class ContextModel {

    private final Node root;
    private final Node escape;
    private final Node eof;
    private final Exclusion exclusions;

    public ContextModel() {
        root = new Node((byte) 0);
        escape = new Node((byte) 0, 1);
        eof = new Node((byte) 0, 1);
        exclusions = new Exclusion();

    }

    // https://www.vutbr.cz/www_base/zav_prace_soubor_verejne.php?file_id=117690
    //simple implementation, not using vines
    public void addSymbol(Context context, byte symbol) {
        int k = context.order();
        for (int i = 0; i <= k; i++) {
            addSubcontext(context, i, k, symbol);
        }
    }

    private void addSubcontext(Context context, int off, int k, byte symbol) {
        Node current = root;
        Node next;

        for (int i = off; i < k; i++) {
            next = findChild(current, context.get(i));

            if (next == null) {
                next = addChild(current, context.get(i)); // add symbol --> first appereance
            }
            current = next;
        }

        // add symbol to the tree
        next = findChild(current, symbol);

        if (next == null) {
            next = addChild(current, symbol);
        }
        next.count++;
    }

    //find node for given symbol in list of children
    private Node findChild(Node parent, byte symbol) {
        Node node = parent.firstChild;

        while (node != null) {
            if (node.symbol == symbol) {
                return node;
            }
            node = node.nextSibling;
        }

        return null;
    }

    protected Node addChild(Node parent, byte symbol) {
        Node node, newn;
        node = parent.firstChild;

        if (node == null) { //no children for node
            newn = new Node(symbol, parent);
            parent.firstChild = newn;
            return newn;
        }

        // go through list
        newn = node.nextSibling;
        while (newn != null) {
            node = newn;
            newn = node.nextSibling;
        }
        //add sibling at the and of list
        newn = new Node(symbol, parent);
        node.nextSibling = newn;
        return newn;

    }

    /*
     * return the list of nodes for a given context
     */
    public List<Node> getNodes(Context context) {
        Node current = root;
        Node next;

        for (int i = 0; i < context.order(); i++) {
            next = findChild(current, context.get(i));

            if (next == null) {
                return null;
            }
            current = next;
        }

        List<Node> nodes = new ArrayList<>();
        Node node = current.firstChild;

        while (node != null) {
            if (!exclusions.isExcluded(node.symbol)) {
                nodes.add(node);
            }
            node = node.nextSibling;
        }

        //|A|ck \{e}| --> PPMC
        if (nodes.isEmpty()) {
            escape.count = nodes.size();
        } else {
            escape.count = 1;
        }

        nodes.add(escape);
        return nodes;
    }

    public Node getESC(List<Node> nodes) {
        return nodes.get(nodes.size() - 1);
    }

    public Node getEOF() {
        return eof;
    }

    public boolean isESC(Node node) {
        return node == escape;
    }

    public boolean isEOF(Node node) {
        return node == eof;
    }

    public Node searchNode(List<Node> nodes, byte symbol) {
        for (Node node : nodes) {
            if (node.symbol == symbol && node != eof && node !=escape) {
                return node;
            }
        }
        return null;
    }

    /*get a list with all the possible characters*/
    public List<Node> getAlphabet() {
        List<Node> nodes = new ArrayList<>();

        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; i++) {
            if (exclusions.isExcluded((byte) i)) {
                continue;
            }
            nodes.add(new Node((byte) i, 1));
        }
        nodes.add(escape);
        nodes.add(eof);

        return nodes;
    }

    //exclude symbols in given context
    public void excludeSymbols(List<Node> nodes) {
        nodes.stream().filter((node) -> (node != escape && node != eof)).forEachOrdered((node) -> {
            exclusions.exclude(node.symbol);
        });

    }
    
    public void clearExludes(){
        exclusions.clear();
    }

    

}
