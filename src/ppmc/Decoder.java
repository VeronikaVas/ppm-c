/*
Base class for a decompressor based on the PPM model and Huffman
 */
package ppmc;

import de.tivano.flash.swf.common.BitInputStream;
import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.PriorityQueue;

/**
 *
 * @author Veronika Vasinova
 */
public class Decoder extends FilterInputStream {

    private final Context context;
    private final ContextModel tree;
    private boolean isEOF = false;
    private final BitInputStream bin;

    public Decoder(InputStream in, int order) {
        super(in);
        context = new Context(order);
        tree = new ContextModel();
        bin = new BitInputStream(in);

    }

    @Override
    public int read() throws IOException {
        byte[] buff = new byte[1];
        int len = read(buff, 0, 1);
        if (len < 1) {
            return -1;
        }
        return buff[0];
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        List<Node> nodes;
        tree.clearExludes();
        int i = 0, max = len, read = 0;
        Context c;
        if (isEOF) {
            return -1;
        }

        while (i < max) {
            Node node;
            c = context.clone();
            byte symbol;
            tree.clearExludes();
            while (true) {
                nodes = tree.getNodes(c);
                node = decodeChar(nodes);

                if (tree.isESC(node)) {
                    tree.excludeSymbols(nodes);

                    if (c.order() == 0) {
                        nodes = tree.getAlphabet();
                        node = decodeChar(nodes);

                        if (tree.isEOF(node) || tree.isESC(node)) {
                            isEOF = true;
                            return read;
                        } else {
                            symbol = node.symbol;
                        }
                        break;
                    } else {
                        c.reduce();
                    }
                } else {
                    symbol = node.symbol;
                    break;
                }
            }

            tree.addSymbol(context, symbol);
            context.append(symbol);

            b[i] = symbol;
            i++;
            read++;
        }

        return read;
    }

    /* 
    *Huffman with PriorityQueue
    https://courses.cs.washington.edu/courses/cse143x/15au/lectures/huffman/huffman.pdf */
    public Node decodeChar(List<Node> nodes) throws IOException {
        PriorityQueue<HuffmanNode> q = new PriorityQueue<>();

        initialize(nodes, q);

        while (q.size() > 1) {
            q.add(new HuffmanNode(q.poll(), q.poll()));
        }

        HuffmanNode htree = q.poll();
        HuffmanNode decoded = down(htree);

        return decoded.node;
    }

    protected void initialize(List<Node> nodes, PriorityQueue<HuffmanNode> queue) {
        nodes.forEach((node) -> {
            queue.add(new HuffmanNode(node));
        });
    }

    /*
      descend through the Huffman tree according to the input bits
     */
    protected HuffmanNode down(HuffmanNode node) throws IOException {
        boolean bit;
        bin.mark(1);
        try {
            bit = bin.readBit();

        } catch (EOFException e) {
            if (node.node == null) {
                throw new IOException("Unexpected EOF!");
            }
            return node;
        }

        if (bit) {
            if (node.right == null) {
                bin.reset();
                return node;
            }
            return down(node.right);
        } else {
            if (node.left == null) {
                bin.reset();
                return node;
            }
            return down(node.left);
        }
    }

    @Override
    public void close() throws IOException {
        bin.close();
        super.close();
    }

}
