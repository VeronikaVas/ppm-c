/*
Base class for a compressor based on the PPM model and Huffman
 */
package ppmc;

import de.tivano.flash.swf.common.BitOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.PriorityQueue;

/**
 *
 * @author Veronika Vasinova
 */
public class Encoder extends FilterOutputStream {

    private final Context context;
    private final ContextModel tree;
    private final BitOutputStream bout;

    public Encoder(OutputStream out, int order) {
        super(out);
        context = new Context(order);
        tree = new ContextModel();
        bout = new BitOutputStream(out);
    }

    @Override
    public void write(int b) throws IOException {
        byte[] buff = new byte[1];
        buff[0] = (byte) b;
        write(buff, 0, 1);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        List<Node> nodes;
        tree.clearExludes();
        Node enc;
        Context c;
        int i = 0, max = len;

        while (i < max) {
            byte symbol = b[i];
            c = context.clone(); // https://dzone.com/articles/java-cloning-copy-constructor-vs-cloning
            tree.clearExludes();

            while (true) {
                //get the list of characters for the current context and find symbol to be compressed
                nodes = tree.getNodes(c);
                enc = tree.searchNode(nodes, symbol);

                if (enc == null) {
                    tree.excludeSymbols(nodes);
                    enc = tree.getESC(nodes);
                    encodeSymbol(nodes, enc);

                    if (c.order() == 0) {
                        nodes = tree.getAlphabet();
                        enc = tree.searchNode(nodes, symbol);
                        encodeSymbol(nodes, enc);
                        break;
                    } else {
                        c.reduce(); //reduce context
                    }
                } else {

                    encodeSymbol(nodes, enc);
                    break;
                }
            }
            tree.addSymbol(context, symbol); //add symbol to the tree
            context.append(symbol);
            i++;
        }
    }

    /* 
    *Huffman with PriorityQueue
    https://courses.cs.washington.edu/courses/cse143x/15au/lectures/huffman/huffman.pdf 
    https://github.com/junjunguo/AlgorithmsDataStructures/blob/master/dataStructures/HuffmanTree.java*/
    public void encodeSymbol(List<Node> nodes, Node encode) throws IOException {
        HuffmanNode n;
        PriorityQueue<HuffmanNode> q = new PriorityQueue<>();

        n = initialize(nodes, q, encode);

        while (q.size() > 1) {
            q.add(new HuffmanNode(q.poll(), q.poll()));
        }

        up(n);
    }

    private HuffmanNode initialize(List<Node> nodes, PriorityQueue<HuffmanNode> q, Node encode) {
        HuffmanNode n = null, t;
        for (Node node : nodes) {
            t = new HuffmanNode(node);
            q.add(t);
            if (node == encode) {
                n = t;
            }
        }
        return n;
    }

    private void up(HuffmanNode node) throws IOException {
        if (node.parent != null) {
            up(node.parent);
            if (node.parent.left == node) {
                bout.writeBit(false);
            }
            if (node.parent.right == node) {
                bout.writeBit(true);
            }
        }

    }

    @Override
    public void close() throws IOException {
        //we have to send EOF
        // https://www2.cs.duke.edu/csed/curious/compression/adaptivehuff.html
        List<Node> nodes;
        tree.clearExludes();
        Node enc;

        //go to the top of tree
        while (true) {
            nodes = tree.getNodes(context);
            enc = tree.getESC(nodes);
            tree.excludeSymbols(nodes);
            encodeSymbol(nodes, enc);
            if (context.order() > 0) {
                context.reduce();
            } else {
                break;
            }
        }

        nodes = tree.getAlphabet();
        enc = tree.getEOF();
        encodeSymbol(nodes, enc);

        //close streams
        bout.close();
        out.close();
    }

}
