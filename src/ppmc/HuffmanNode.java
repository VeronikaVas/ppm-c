package ppmc;

/**
 *
 * @author Veronika
 */
public class HuffmanNode implements Comparable {

    Node node = null;
    int count;

    HuffmanNode parent = null;
    HuffmanNode left = null;
    HuffmanNode right = null;

    public HuffmanNode(HuffmanNode l, HuffmanNode r) {
        this.left = l;
        this.right = r;

        left.parent = this;
        right.parent = this;

        count = left.count + right.count;
    }

    public HuffmanNode(Node node) {
        this.node = node;
        this.count = node.count;
    }

    @Override
    public int compareTo(Object arg) {
        if (arg == null) {
            throw new NullPointerException();
        }

        HuffmanNode n2 = (HuffmanNode) arg;

        if (this.count < n2.count) {
            return -1;
        }

        if (this.count > n2.count) {
            return 1;
        }

        return 0;
    }

    @Override
    public String toString() {
        return "Node" + node; 
    }

}
