/*
 * Node for context tree.
 */
package ppmc;

/**
 *
 * @author Veronika Vasinova
 */
public class Node {

    byte symbol;
    Node parent = null;
    Node nextSibling = null;
    Node firstChild = null;
    int count = 0;

    public Node(byte s) {
        this.symbol = s;
    }

    public Node(byte s, int c) {
        this.symbol = s;
        this.count = c;
    }
    
    public Node(byte s, Node p) {
        this.symbol = s;
        this.parent = p;
    }

    @Override
    public String toString() {
        return "Node{" + "symbol=" + (char) symbol + ", count=" + count + '}';
    }

}
