package ppmc;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Veronika Vasinova
 */
public class PPMC {

    private final static int BUFF = 2048;
    private final static int K = 6;

    public static void main(String[] args) {
        InputStream in;
        OutputStream out;
        FileInputStream inFile;
        FileOutputStream outFile;

        if (args.length == 3) {
            try {
                inFile = new FileInputStream(args[1]);
                outFile = new FileOutputStream(args[2]);

                in = new BufferedInputStream(inFile);
                out = new BufferedOutputStream(outFile);
            } catch (FileNotFoundException e) {
                System.err.println("No such a file!");
                System.exit(1);
                return;
            }

            choice(args[0], in, out);

            try {
                in.close();
                out.close();
            } catch (IOException e) {
                System.err.println("");
            }
        }

        if (args.length == 1 && args[0].equals("-h")) {
            helpMessage();
        }

    }

    public static void choice(String ch, InputStream in, OutputStream out) {
        try {
            switch (ch) {
                case "-c":
                    System.out.println("Compressing...");
                    compress(in, out);
                    System.out.println("Done!");
                    break;

                case "-d":
                    System.out.println("Decompressing...");
                    decompress(in, out);
                    System.out.println("Done!");
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(PPMC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void helpMessage() {
        System.out.println("Usage:   java -jar ppmc.jar [-c|-d|-h] [file_in] [file_out]");
        System.out.println("Where:   c - compress");
        System.out.println("         d - decompress");
        System.out.println("         h - help");
    }

    public static void compress(InputStream in, OutputStream out) throws IOException {
        Encoder enc;
        enc = new Encoder(out, K);

        //http://tutorials.jenkov.com/java-io/inputstream.html
        byte[] buf = new byte[BUFF];
        int read;
        read = in.read(buf);
        while (read > 0) {
            enc.write(buf, 0, read);
            read = in.read(buf);
        }
        enc.close();
    }

    public static void decompress(InputStream in, OutputStream out) throws IOException {
        Decoder dec;
        dec = new Decoder(in, K);

        byte[] buf = new byte[BUFF];
        int read;

        //http://tutorials.jenkov.com/java-io/outputstream.html
        //http://www.java2s.com/Code/Java/File-Input-Output/UseBufferedInputStreamandBufferedOutputStreamtocopybytearray.htm
        read = dec.read(buf);
        while (read > 0) {
            out.write(buf, 0, read);
            read = dec.read(buf);
        }
        dec.close();
    }

}
